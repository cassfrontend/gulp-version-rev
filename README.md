#gulp-version-rev

批量给链接添加版本号(?version=0.0.1), 可以自定义匹配规则

### 安装

```
npm install git+https://gitee.com/cassfrontend/gulp-version-rev.git#1.4.1 --save
```

### 使用

```javascript
var gulp = require('gulp');
var versionRev = require('gulp-version-rev');
gulp.task('rev', function(done){
    gulp.src('src/*.ftl').pipe(versionRev({
      version: '0.0.0',
       ignoreUrls:[/^http:\/\//,'test.png']
    }))
});
```

### API
versionRev(options)

#### options
    
    version, String，版本号，如果不填写默认为当前时间戳
    ignoreUrls, Array, 忽略的URL，支持正则和字符串，匹配的URL不添加版本号
    preprocess, 如果匹配，会用uuid替换该文本，完成所有操作后恢复文本
    
### 自定义规则

  目前只支持.ftl,.html后缀，可以自定义匹配规则，默认options如下
  
  ```javascript
      const DEFAULT_OPTIONS = {
      
        '.ftl': [
          {reg: /<script.*?\s+src="(.+?\.js)"(\s+|>)/g, pos: 1},
          {reg: /<script.*?\s+src="(<@ofbizContentUrl>.+?\.js<\/@ofbizContentUrl>)"(\s+|>)/g, pos: 1},
          {reg: /<link.*?\s+href="(.+?\.css)"(\s+|>)/g, pos: 1},
          {reg: /<link.*?\s+href="(<@ofbizContentUrl>.+?\.css<\/@ofbizContentUrl>)"(\s+|>)/g, pos: 1},
          {reg: /<img.*?\s+src="(.+?)"(\s+|\/>|>)/g, pos: 1}
        ],
        '.html': [
          {reg: /<script.*?\s+src="(.+?\.js)"(\s+|>)/g, pos: 1},
          {reg: /<link.*?\s+href="(.+?\.css)"(\s+|>)/g, pos: 1},
          {reg: /<img.*?\s+src="(.+?)"(\s+|\/>|>)/g, pos: 1}
        ],
        ignoreUrls: [],
        preprocess: {
          '.ftl': [/\$\{.+?}/g]
        },
        version: new Date().getTime()
      };
    
  ```